//
// Created by aniko on 2020. 02. 07..
//

#include <3rd-party/catch/catch.hpp>

#include <list>
#include <iostream>
#include "encoder.h"
#include <QString>
#include <QRegularExpression>

using namespace std;

Encoder encoder;

void init(string str) {
    readJson(QString::fromStdString(str), encoder);
}

TEST_CASE("readjson") {

    SECTION("read ok") {
        REQUIRE(readJson("../../test/js3.js",encoder) == 1);
    };
    SECTION("read nope") {
        REQUIRE(readJson("../../test/js.js",encoder) == 0);
    };
}

TEST_CASE("readfile") {

    QString str="";
    SECTION("read ok") {
        REQUIRE(readFile("../../test/js3.js",str)==1);
    };
    SECTION("read nope") {
        REQUIRE(readFile("../../test/js.js",str)==0);
    };
}

TEST_CASE("js.js") {
    init("../../test/js.js");

    SECTION("encode") {
        REQUIRE(encoder.encode("abc") == "1 2 3 ");
    };
    SECTION("decode ") {
        REQUIRE(encoder.decode("1 2 3 ") == "abc");
    };
}

TEST_CASE("js2.js") {
    init("../../test/js2.js");

    SECTION("encode ok") {
        REQUIRE(encoder.encode("abc") == "123");
    };
    SECTION("encode nope") {
        REQUIRE(encoder.encode("abcd") == "");
    };
    SECTION("decode ok") {
        REQUIRE(encoder.decode("123") == "abc");
    };
    SECTION("decode nope") {
        REQUIRE(encoder.decode("1234") == "");
    };
}


