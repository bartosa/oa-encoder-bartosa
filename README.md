# Decoder

## Docker

Docker build és indítás (-ti kapcsolóval) után a build mappán belül az src mappába navigálva indítható a _./decoder_ program.

## Használat:

1.  **Json file megadása** <br> 
    Egy olyan json file megadása, ami tartalmazza a kódtáblát.
2.  **Parancsok bevitele** <br> 
    A szoftverben az alábbi, kódoláshoz és dekódoláshoz kapcsolódó parancsok adhatók:
    *   _ENCODE_ : A parancs utáni karaktersorozatot kódolja le. (Szóköz szükséges a parancs után) Pl: ENCODE almafa, az eremény: 112233114411 
    *   _DECODE_ : Az encode-hoz hasonlóan működik, csak itt, a kódolt szöveget fejti vissza a program: Pl: DECODE 112233114411, az eredmény: almafa
    *   _ENCODE_FILE_ : Bemenete két fájl helye, ahol az elsőt kódolja le a másodikba. Pl. ENCODE_FILE input.txt output.txt. Ekkor a kódolt szöveg az output.txt-be íródik
    *   _DECODE_FILE_ : Hasonlóan működik, mint az "ENCODE_FILE", viszont itt kódolt szöveg visszafejtése történik.
3.  **Kilépés** <br>
    Kilépéshez parancsként az "EXIT"-et kell megadni. (Csupa nagybetűvel) Ekkor a szoftver kiiírja az alábbi üzenetet: "KTHXBYE", majd kilép.  



