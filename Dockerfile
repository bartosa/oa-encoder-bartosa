# imagename: ubuntu
FROM ubuntu

RUN apt-get update && apt-get -y install cmake g++ gcc qt5-default qt5-qmake

#src mappa tartalmát  a hack/src-be tesz
COPY src/. /decoder/src
COPY CMakeLists.txt /decoder
COPY README.md /decoder
COPY 3rd-party/. /decoder/3rd-party
COPY modules/. /decoder/modules
COPY test/. /decoder/test

RUN mkdir -p /decoder/build

WORKDIR /decoder/build

RUN cmake ..
RUN make


#EXPOSE

ENTRYPOINT ["/bin/bash"]





