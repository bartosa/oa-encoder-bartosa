//
// Created by aniko on 2020. 01. 31..
//

#include "encoder.h"


Encoder::Encoder() {

}

QString Encoder::encode(QString str) {

    QString ret = "";

    for (QChar c :str) {
        if (c2code.find(c.toLower()) != c2code.end()) {
            ret += c2code[c.toLower()];
        } else {
            cerr << "INVALID INPUT" << endl;
            return "";
        }
    }

    return ret;
}

void Encoder::encodeFile(QString from, QString to) {

    QString text = "";
    if (readFile(from, text) != 0) {
        cout << "INVALID INPUT FILE" << endl;
    } else {
        QString result = encode(text);
        ofstream myfile(to.toStdString());
        if (myfile.is_open() && result != "") {
            myfile << result.toStdString();
            myfile.close();
            cout << "Encode done!" << endl;
        } else if (result == "") {
            // INVALID INPUT msg
        } else {
            cout << "INVALID OUTPUT FILE: " << to.toStdString() << endl;
        }
    }
}

QString Encoder::decode(QString str) {

    return decrypt(pair<QString, QString>("", str)).first;
}

void Encoder::decodeFile(QString from, QString to) {

    QString text = "";

    if (readFile(from, text) != 0) {
        cout << "INVALID INPUT FILE" << endl;
    } else {
        ofstream myfile(to.toStdString());
        QString result = decode(text);
        if (myfile.is_open() && result != "") {
            myfile << result.toStdString();
            myfile.close();
            cout << "Decode done!" << endl;
        } else if (result == "") {
            // INVALID INPUT msg
        } else {
            cerr << "INVALID OUTPUT FILE" << to.toStdString() << endl;
        }
    }
}

pair<QString, QString> Encoder::decrypt(pair<QString, QString> p) {// decoded sqring (build), coded string (cut)

    if (p.second.length() == 0) {
        return p;
    }

    if (p.second.length() > 0) {
        int i = 0;
        while (i < 10 && p.second.length() != 0 && i <= p.second.length()) {
            if (code2c.find(p.second.mid(0, i)) != code2c.end()) { // if find int the code table
                p.first += code2c[p.second.mid(0, i)]; //build the decoded text
                p.second = p.second.mid(i, p.second.size() - 1); // cut the coded text

                if (p.second.length() != 0) {
                    return decrypt(p);
                }

            } else {
                i++;
            }
        }
        if (p.second.length() > 0) { // if there are remain part:
            cerr << "INVALID INPUT" << endl;
            p.first = "";
            p.second = "";
        }
    }

    return p;
}

int readJson(QString jPath, Encoder &e) {

    e.c2code.clear();
    e.code2c.clear();
    QString fileBody = "";

    if (readFile(jPath, fileBody) == 1 || fileBody == "") {
        cerr << "ERROR: Invalid coding table" << endl;
        return 1;
    } else {
        try {
            json jBody = json::parse(fileBody.toStdString()); // NOTE: this could throw an exception

            for (const auto &item : jBody.items()) {
                e.c2code[QString::fromStdString(item.key()).toLower()] = QString::fromStdString(item.value());
                e.code2c[QString::fromStdString(item.value())] = QString::fromStdString(item.key()).toLower();
            }
        }
        catch (...) {
            cerr << "ERROR: Invalid coding table" << endl;
            return 1;
        }
        return 0;
    }
}

int readFile(QString filePath, QString &body) {

    string line;
    ifstream myfile(filePath.toStdString());

    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            body += QString::fromStdString(line);
        }
        myfile.close();
        return 0;
    }

    return 1;
}
