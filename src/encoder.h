//
// Created by aniko on 2020. 01. 31..
//

#ifndef DECODER_ENCODER_H
#define DECODER_ENCODER_H

#include <QtCore/QString>
#include <QMap>
#include <fstream>
#include <iostream>
#include <string>
#include <json.hpp>
#include <QPair>

using namespace std;

using namespace nlohmann;


class Encoder {

public:
    Encoder();

    QString encode(QString str);

    void encodeFile(QString from, QString to);

    QString decode(QString str);

    void decodeFile(QString from, QString to);

public:
    QMap<QString, QString> c2code; //letter-code
    QMap<QString, QString> code2c; //code- letters

private:

    pair<QString, QString> decrypt(pair<QString, QString> p);
};

int readJson(QString jPath, Encoder &e);
int readFile(QString filePath, QString &body);

#endif //DECODER_ENCODER_H
