#include <iostream>
#include "encoder.h"
#include <QTextStream>
#include <QRegularExpression>
#include <chrono>
#include <thread>

using namespace std;


int main() {
    cout << "***Encoder/Decoder***" << endl << endl;

    Encoder encoder;

    string jPath = "";
    int ret = 1;
    while (ret != 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50)); //error message too fast
        cout << "Json file: ";
        cin >> jPath;
        ret = readJson(QString::fromStdString(jPath), encoder);
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    cout << "Json read SUCCES" << endl;

    QTextStream stream(stdin);
    QString line;
    QString command = "";

    do {
        cout << "New command:" << endl;
        line = stream.readLine();
        try {
            command = line.mid(0, line.indexOf(" "));
            QString text = line.mid(line.indexOf(" ") + 1);

            if (command == "ENCODE") {
                cout << encoder.encode(text).toStdString() << endl;
            }
            if (command == "DECODE") {
                cout << encoder.decode(text).toStdString() << endl;
            }
            if (command == "ENCODE_FILE") {
                encoder.encodeFile(text.mid(0, text.indexOf(" ")), text.mid(text.indexOf(" ") + 1));
            }
            if (command == "DECODE_FILE") {
                encoder.decodeFile(text.mid(0, text.indexOf(" ")), text.mid(text.indexOf(" ") + 1));
            }
        } catch (...) {
            cout << "Error while command" << endl;
        }
    } while (command != "EXIT");

    cout << "KTHXBYE" << endl;

    return 0;
}
